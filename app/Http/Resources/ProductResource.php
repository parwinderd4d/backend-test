<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'product' => $this->productName,
            'product_line' => $this->productline->productLine,
            'unit_price' => (float) $this->orderdetails->priceEach,
            'qty' => $this->orderdetails->quantityOrdered,
            'line_total' => $this->calculateTotalPrice(),
        ];
    }
}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\OrderResource;
use App\Models\Order;

class OrdersController extends Controller
{

    public $successStatus   = 200;
    public $unauthorised    = 401;
    public $badRequest      = 400;
    public $notFound        = 404;

    /**
     * fetch order details
     * @param $id
     * @return array
     */
    public function fetchOrderData($id)
    {
        // your logic goes here.
        try {

            if (Order::find($id)) {
                $order = new OrderResource(Order::orderDetails($id));
                return response($order,$this->successStatus);
            } else {
                return response()->json([
                    "message" => "No Record Found!"
                ], $this->badRequest);
            }


        } catch (\Exception $e) {
            return response()->json(array('status' => 'error', 'message' =>   $e->getMessage()), $this->badRequest);
        }

    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'customers';

    /**
    * primary key of table
    *
    * @var string
    */
    protected $primaryKey = 'customerNumber';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */

    protected $fillable = [
        'customerNumber',
        'customerName',
        'contactLastName',
        'contactFirstName',
        'phone',
        'addressLine1',
        'addressLine2',
        'city',
        'state',
        'postalCode',
        'country',
        'salesRepEmployeeNumber',
        'creditLimit',
    ];
 

     /**
      * Customer table has many orders
      */
     public function orders()
     {
         return $this->hasMany('App\Models\Order', 'customerNumber', 'customerNumber');
     }
 
}

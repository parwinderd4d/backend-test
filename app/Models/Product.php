<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'products';


    /**
     * The primary key associated with the table.
     *
     * @var string
     */
    protected $primaryKey = 'productCode';

    /**
     * Get productline data as string using inverse relationship with product table
     */
    public function productline()
    {
        return $this->belongsTo('App\Models\Productline', 'productLine', 'productLine');
    }

    /**
     *
     */
    public function orders()
    {
        return $this->belongsToMany('App\Models\Order', 'orderdetails', 'productCode', 'orderNumber')
            ->withPivot('quantityOrdered', 'priceEach', 'orderLineNumber')
            ->as('orderdetails');
    }


    public function calculateTotalPrice(){

        $price = $this->orderdetails->priceEach;
        $qty = $this->orderdetails->quantityOrdered;
        $totalPrice = $price * $qty;
        return number_format(($totalPrice),2,'.','');

    }
}


<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Productline extends Model
{

    
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'productlines';

    /**
     * The primary key associated with the productlines table.
     *
     * @var string
     */
    protected $primaryKey = 'productLine';

    /**
     * The primary key of productlines table is not an integer
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Get the products that belong to this product line.
     */
    public function products()
    {
        return $this->hasMany('App\Models\Product', 'productLine', 'productLine');
    }
}
<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
    * The table associated with the model.
    *
    * @var string
    */
    protected $table = 'orders';

    /**
    * primary key of tale
    *
    * @var string
    */
    protected $primaryKey = 'orderNumber';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */

    protected $fillable = [
        'orderNumber',
        'orderDate',
        'requiredDate',
        'shippedDate',
        'status',
        'comments',
        'customerNumber',
    ];

     /**
     * products and orders table PK contain orderdetails table
     * access product table directly through orders table
     */
     public function products()
     {
         return $this->belongsToMany('App\Models\Product', 'orderdetails', 'orderNumber', 'productCode')
             ->withPivot('quantityOrdered', 'priceEach', 'orderLineNumber')
             ->as('orderdetails');
     }
 
     /**
      * Inverse relationship of customer table
      *
      */
     public function customer()
     {
         return $this->belongsTo('App\Models\Customer', 'customerNumber', 'customerNumber');
     }
 
     /**
      * Calculate the total bill amount of each product price
      * Access this method in order Resource
      *
      * @return string
      */
     public function productsTotalBillAmount()
     {
         $billAmount = 0;
         if (count($this->products) > 0) {
             foreach($this->products as $product)
                 $billAmount = $billAmount + ( $product->orderdetails->quantityOrdered * $product->orderdetails->priceEach);
         }
         return number_format($billAmount, 2, '.', '');
     }
 
     /**
      * fetch all order data by orderNumber id
      *
      * @return
      */
     public static function orderDetails($orderNumber) {
         return self::with(['products', 'customer'])
             ->where('orderNumber', '=', $orderNumber)
             ->firstOrFail();
     }
 
}
